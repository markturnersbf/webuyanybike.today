<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 */
  
define( 'WP_MEMORY_LIMIT', '4096M' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A,PY.d-MlS<b6(0REs[,CM(.G_icY1K}qp$}LF.`mt%QEwAREPE77TDErF7nT_+R');
define('SECURE_AUTH_KEY',  'E4&Ik@rN|zIpTS=~~~5%9L2k0E@`|>?$:k>Y#,9 1-dmMMpb`tvyL*&GF]?YKYu%');
define('LOGGED_IN_KEY',    '^rYM+w@NVYgB|efP7/m!^}~r3+l!w@m]L$TA8+,qxA;x,NZ +Gw+RxQ@aTZm=dK}');
define('NONCE_KEY',        '!@ouPG-;OBO~EGyMiNN 5Xy?|qEX@O+6P!tg6^mPbn<+^|Y3W=k|3<WBmKV3*H)&');
define('AUTH_SALT',        'f[^3diEU#(U-i;X7UGOR-.@/)-6>^T|6-z148bH?cNGGQ$n5>#g]i7+<xhzsAJAi');
define('SECURE_AUTH_SALT', 'JD_2gR=y4wYeUK*S~AR;+T*ITBLm)]UNO/%?n;$WIbmaIIg<Aws]rG22(-=C(!wo');
define('LOGGED_IN_SALT',   'l)TAc suicY[reBpiOO)qSS2SmjM_7mTPC?/8rY`c[:%Bya=.04)T@~3G=JHY*iB');
define('NONCE_SALT',       '5=Em/G4D2ZJ0LY4;-(`y8cMJU]~Ri*4v3i4Be-v|-+r+-ukr^F*3.=RCW)>a$J#Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 *  Change this to true to run multiple blogs on this installation.
 *  Then login as admin and go to Tools -> Network
 */
define('WP_ALLOW_MULTISITE', false);
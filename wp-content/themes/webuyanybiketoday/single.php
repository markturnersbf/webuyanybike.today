<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="content-wrapper">
		<div id="main-content" role="main">
			<div id="row" class="blank">
		<div id="inner-container" class="blog-wrap">
			<?php
				get_sidebar();
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					// Previous/next post navigation.
					twentyfourteen_post_nav();
					
				endwhile;
				 ?>
			
				</div>
			</div>
			
			<div id="row" class="">
				<div id="inner-container">
				<div class="reg-wrapper standalone">
					<p class="reg-intro">Enter your reg number to get a <span>FREE</span> valuation...</span></p>
					<form id="reg-input" action="<?php echo home_url(); ?>/enquiry-form/">
						<div class="reg-input">
							<input type="text" name="regnum" />
						</div>
						<input type="submit" value="value my bike &raquo;" />
					</form>
					<p class="dont-know">Don't know your bike's registration number? <a href="<?php echo home_url(); ?>/enquiry-form/">Click here &raquo;</a></p>
				</div>
			</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();

<?php

/* custom PHP functions below this line */


//Shortcodes go here
add_filter('widget_text', 'do_shortcode');


//[row]

function row($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'style' => ''
	), $params));
	$row = '
		<div id="row" class="'.$style.'">
			<div id="inner-container">
			'.$content.'
			</div>
		</div>';
	return $row;
}

//[column]

function column($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'style' => ''
	), $params));
	$column = '
		<div id="column" class="'.$style.'">
			'.$content.'
		</div>';
	return $column;
}

//[cta]

function cta($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'text' => 'CTA Text &raquo;',
		'link' => '#',
		'style' => 'default',
	), $params));
	$cta = '
		<a id="cta" class="'.$style.'" href="'.$link.'" title="'.$text.'">'.$text.'</a>';
	return $cta;
}

//[floated_image]

function floated_image($params = array(), $content = null) {
	$content = do_shortcode($content);
	$floated_image = '
		<span class="floated-image">'.$content.'</span>';
	return $floated_image;
}

//[steps]

function steps($params = array(), $content = null) {
	$content = do_shortcode($content);
	$steps = '
		<span class="steps">'.$content.'</span>';
	return $steps;
}

//[no_haggle]

function no_haggle($params = array(), $content = null) {
	$content = do_shortcode($content);
	$no_haggle = '
		<div class="no_haggle">
			<div class="haggle-wrapper">
				<img src="'.get_stylesheet_directory_uri().'/images/pointing-dude.png" class="pointing-dude" />
				<div class="haggle-content">
					'.$content.'
					<a id="cta" href="'.get_home_url().'/enquiry-form/" title="Sell my bike">sell my bike &raquo;</a>
				</div>
			</div>
			<img class="motorbike-trail" src="'.get_stylesheet_directory_uri().'/images/bikes.png" />
		</div>';
	return $no_haggle;
}

//[reg_lookup_home]

function reg_lookup_home($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'title' => '',
	), $params));
	$reg_lookup_home = '
				<h1 class="home">'.$title.'</h1>
				<div class="reg-wrapper">
					<p class="reg-intro">Enter your reg number to get a <span>FREE</span> valuation...</span></p>
					<form id="reg-input" action="'.get_home_url().'/enquiry-form/">
						<div class="reg-input">
							<input type="text" name="MXIN_VRM">
						</div>
						<input type="submit" value="value my bike &raquo;" />
					</form>
					<p class="dont-know">Don&#39;t know your bike&#39;s registration number? <a href="'.get_home_url().'/enquiry-form/">Click here &raquo;</a></p>
				</div>
				<div class="coloured-tiles">
					<div class="tile one">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-1.png" />
						<p>Free Valuation of Your Bike</p>
					</div>
					<div class="tile two">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-2.png" />
						<p>Free Collection UK Mainland</p>
					</div>
					<div class="tile three">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-3.png" />
						<p>No Haggle<br />Policy<br /></p>
					</div>
					<div class="tile four">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-4.png" />
						<p>Bikes bought within 24hrs</p>
					</div>
					<div class="tile five">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-5.png" />
						<p>Instant Bank Transfer</p>
					</div>
				</div>

	';
	return $reg_lookup_home;
}

//[tile_only]

function tile_only($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'style' => '',
		'title' => '',
	), $params));
	$tile_only = '
		<div id="row" class="'.$style.'">
			<div id="inner-container">
				<h2>'.$title.'</h2>
				<div class="coloured-tiles">
					<div class="tile one">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-1.png" />
						<p>Free Valuation of Your Bike</p>
					</div>
					<div class="tile two">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-2.png" />
						<p>Free Collection UK Mainland</p>
					</div>
					<div class="tile three">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-3.png" />
						<p>No Haggle<br />Policy</p>
					</div>
					<div class="tile four">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-4.png" />
						<p>Bikes bought within 24hrs</p>
					</div>
					<div class="tile five">
						<img src="'.get_stylesheet_directory_uri().'/images/coloured-5.png" />
						<p>Instant Bank Transfer</p>
					</div>
				</div>
			</div>
		</div>

	';
	return $tile_only;
}

//[contact]

function contact($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'style' => '',
		'title' => '',
	), $params));
	/*$contact = '
		<div class="contact-wrapper">
			<p class="email"><a href="mailto:info@wewantbikes.com">info@wewantbikes.com</a></p>
			<p class="address">Cottage Street Mill, Cottage Street, Macclesfield SK11 8DZ.</p>
			<p class="phone">01625 353 014</p>
			<p class="hours">Mon-Fri: 8am-6pm | Sat: 10am-5pm | Sun: Closed</p>
		</div>
	';*/
	$contact = '
		<div class="contact-wrapper">
			<p class="email"><a href="mailto:info@webuyanybike.today">info@webuyanybike.today</a></p>
			<p class="phone">0330 445 1030</p>
			<p class="hours">Mon-Thurs: 8am-8pm | Fri: 8am-5:30pm | Sat: 8:30am-5pm | Sun: 11am-5pm</p>
		</div>
	';
	return $contact;
}

//[thanks]

function thanks($params = array(), $content = null) {
	$content = do_shortcode($content);
	extract(shortcode_atts(array(
		'style' => '',
		'title' => 'Thank you!',
	), $params));
	$thanks = '
		<div id="row" class="blank thanks-header '.$style.'">
			<div id="inner-container">
				<div class="thank-you">'.$title.'</div>
				'.$content.'
			</div>
		</div>
	';
	return $thanks;
}

add_shortcode('thanks', 'thanks');
add_shortcode('contact', 'contact');
add_shortcode('tile_only', 'tile_only');
add_shortcode('reg_lookup_home', 'reg_lookup_home');
add_shortcode('no_haggle', 'no_haggle');
add_shortcode('steps', 'steps');
add_shortcode('floated_image', 'floated_image');
add_shortcode('cta', 'cta');
add_shortcode('column', 'column');
add_shortcode('row', 'row');

add_filter("the_content", "the_content_filter");

function the_content_filter($content) {

	// array of custom shortcodes requiring the fix
	$block = join("|",array("column", "row", "postcode", "cta", "floated_image", "steps", "no_haggle", "reg_lookup_home", "reg_lookup", "tile_only", "contact", "thanks"));

	// opening tag
	$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);

	// closing tag
	$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);

	return $rep;

}

//----------------------------------------------
//--------------add theme support for thumbnails
//----------------------------------------------

if ( function_exists( 'add_theme_support')){
    add_theme_support( 'post-thumbnails' );
}
add_image_size( 'full-landings', 1600, 595, true);
add_image_size( 'full-blog', 760, 420, true);


//Widget Areas

/*add_action( 'after_setup_theme', 'child_theme_setup' );

if ( !function_exists( 'child_theme_setup' ) ):
function child_theme_setup() {

	register_sidebar( array(
		'name' => __( 'Sidebar Name', 'twentyfourteen' ),
		'id' => 'sidebar-id',
		'description' => __( 'Sidebar Description', 'twentyfourteen' ),
	) );



}
endif;*/

//POSTCODE LOOKUP
add_action("wp_ajax_nopriv_get_address_list", "get_address_list");
add_action("wp_ajax_get_address_list", "get_address_list");

function get_address_list(){

	$searchterm = !empty($_POST['searchkey']) ? $_POST['searchkey'] : '' ;

	include( get_stylesheet_directory() . '/api/PostcodeAnywhere_Interactive_Find_v1_10.php' );
	include( get_stylesheet_directory() . '/includes/config.php' );
	$pa = new PostcodeAnywhere_Interactive_Find_v1_10 (POSTCODE_LOOKUP_KEY,$searchterm ,"English","None",POSTCODE_USER);
	$pa->MakeRequest();
	if ($pa->HasData())
	{
		?>
		<select  class="autoload-sub">
			<option value="">Please select your address....</option>
		<?php $dataflag = 0;
		$data = $pa->HasData();
		foreach ($data as $item)
		{
			echo '<option data-addressid="'.$item["Id"].'"  data-source="'.$_POST['source'].'">'.$item["StreetAddress"].', '.$item["Place"].'</option> ';
		}?>

		</select>
	<?php }
	wp_die();
}

add_action("wp_ajax_nopriv_get_address_detail", "get_address_detail");
add_action("wp_ajax_get_address_detail", "get_address_detail");

function get_address_detail(){

	$address = array();
	include( get_stylesheet_directory() . '/api/PostcodeAnywhere_Interactive_RetrieveById_v1_30.php' );
	include( get_stylesheet_directory() . '/includes/config.php' );
	$pa3 = new PostcodeAnywhere_Interactive_RetrieveById_v1_30 (POSTCODE_LOOKUP_KEY,$_POST['searchkey'],"English",POSTCODE_USER);
	$pa3->MakeRequest();
	if ($pa3->HasData())
	{
			$data3 = $pa3->HasData();
			foreach ($data3 as $item3)
			{
				$address['address1']   = $item3["Line1"].'';
				$address['address2']   = $item3["Line2"].' '.$item3["Line3"];
				$address['postcode']   = $item3["Postcode"].'';
				$address['city']       = $item3["PostTown"].'';
				$address['county']     = $item3["County"].'';
				$address['status']     = 1;
			}
	} else {
			$address = array();
	}

	echo json_encode($address);

	wp_die();
}

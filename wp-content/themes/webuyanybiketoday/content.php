<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if ( 'post' == get_post_type() ) : ?>
				<div id="date-stamp">
					<span class="day"><?php echo get_the_date('j'); ?></span>
					<span class="year"><?php echo substr(get_the_date('F'), 0, 3); ?> <?php echo substr(get_the_date('Y'), 2, 2); ?></span>
				</div>
			<?php endif; ?>

	<header class="blog-headers">
		
		<?php

			if ( is_single() ) :
				the_title( '<h1 class="blog-title">', '</h1>' );
			else :
				the_title( '<h1 class="blog-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
			endif;
		?>

		<div class="entry-meta">
			<span class="author">Published by: <span><?php the_author(); ?></span>
			
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<?php if ( is_single() ) : ?>
	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
			<p class="cat-links"><span>Categories:</span> <?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></p>
		<?php
			endif; ?>
	</div><!-- .entry-content -->
	<?php else : ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
        <a href="<?php echo esc_url( get_permalink()); ?>" title="Read More »" class="blog-readmore">Read more »</a>
		
		
	</div><!-- .entry-summary -->
	<?php endif; ?>
</article><!-- #post-## -->
	

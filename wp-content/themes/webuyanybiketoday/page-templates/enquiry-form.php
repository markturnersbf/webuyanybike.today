<?php
session_start();
// To enable error reporting for debugging
//error_reporting(E_ALL & ~E_NOTICE);
//ini_set('display_errors', true);

//Include the look up function
include("VRMLookupHandler.php");

//Run the look up function
$VRMDetails = VRMLookupHandler();

?>
<?php
/**
 * Template Name: Enquiry Form
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="content-wrapper">
		<div id="main-content" role="main">

			<div id="row" class="blank enquiry-form-top">
				<div id="inner-container">
					<h1>get your motorbike valuation now!</h1>
					<div class="form-header">
						<h2>get paid within 24 hours!</h2>
						<p>Fill out the form below to get your <span>FREE</span> valuation!</p>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/form.png" />
					</div>
				</div>
			</div>
			<div id="row" class="enquiry-form">
				<div id="inner-container">
					<form id="enquiry-form" action="<?php echo get_stylesheet_directory_uri(); ?>/processform-wewantbikes.php" method="post">
						<div id="form-wrapper" class="one">
							<h3><span>1</span>Your Bike Info<span class="legend">* required fields</span></h3>
							<div class="input-wrapper">
								<label>Registration Number <span>*</span></label>
								<input onkeypress="return isAlphaNumberKey(event)" name="reg-num" id="reg-num" type="text" value="<?= $VRMDetails['DATA']['VRM']; ?>" />
								<?= ($VRMDetails['ERROR']) ? $VRMDetails['ERROR']['MSG']:''; ?>

							</div>
							<div class="input-wrapper">
								<label>Make of Bike <span>*</span></label>
<?php if (!empty($VRMDetails['DATA']['Make'])): ?>
								<input type="text" name="make-bike" value="<?= $VRMDetails['DATA']['Make']; ?>" class="required">
<?php else: ?>
								<select class="required" id="make-bike" name="make-bike" onchange="FillModels(this);">
									<option value="">Please Select...</option>
									<option value="Adly">Adly</option>
									<option value="Aeon">Aeon</option>
									<option value="AGS">AGS</option>
									<option value="Apache">Apache</option>
									<option value="Apollo">Apollo</option>
									<option value="Aprilia">Aprilia</option>
									<option value="Ariel">Ariel</option>
									<option value="Bad Island Toyz">Bad Island Toyz</option>
									<option value="Baotian">Baotian</option>
									<option value="Bashan">Bashan</option>
									<option value="Beeline">Beeline</option>
									<option value="Benelli">Benelli</option>
									<option value="Beta">Beta</option>
									<option value="Big Dog">Big Dog</option>
									<option value="Bimota">Bimota</option>
									<option value="BMC">BMC</option>
									<option value="BMW">BMW</option>
									<option value="Bombardier">Bombardier</option>
									<option value="Boom">Boom</option>
									<option value="Boss Hoss">Boss Hoss</option>
									<option value="BSA">BSA</option>
									<option value="BUELL">BUELL</option>
									<option value="BULTACO">BULTACO</option>
									<option value="CAGIVA">CAGIVA</option>
									<option value="CAN-AM">CAN-AM</option>
									<option value="CCM">CCM</option>
									<option value="COBRA">COBRA</option>
									<option value="CPI">CPI</option>
									<option value="DAELIM">DAELIM</option>
									<option value="DAFIER">DAFIER</option>
									<option value="DERBI">DERBI</option>
									<option value="DI-BLASI">DI-BLASI</option>
									<option value="DINLI">DINLI</option>
									<option value="DIRECT BIKE">DIRECT BIKE</option>
									<option value="DKW">DKW</option>
									<option value="DUCATI">DUCATI</option>
									<option value="E-MOTIVE">E-MOTIVE</option>
									<option value="EASY RIDER">EASY RIDER</option>
									<option value="ELECTRICYCLE">ELECTRICYCLE</option>
									<option value="ENFIELD">ENFIELD</option>
									<option value="FOREVER">FOREVER</option>
									<option value="FRANCIS BARNETT">FRANCIS BARNETT</option>
									<option value="GARELLY">GARELLY</option>
									<option value="GAS GAS">GAS GAS</option>
									<option value="GENERIC">GENERIC</option>
									<option value="GIANT CO">GIANT CO</option>
									<option value="GILERA">GILERA</option>
									<option value="GREEVES">GREEVES</option>
									<option value="HAOTIAN">HAOTIAN</option>
									<option value="HARLEY DAVISON">HARLEY DAVISON</option>
									<option value="HI BIRD">HI BIRD</option>
									<option value="HONDA">HONDA</option>
									<option value="HONGDU">HONGDU</option>
									<option value="HUATIAN">HUATIAN</option>
									<option value="HUONIAO">HUONIAO</option>
									<option value="HUSABERG">HUSABERG</option>
									<option value="HUSQVARNA">HUSQVARNA</option>
									<option value="HYOSUNG">HYOSUNG</option>
									<option value="INDIAN">INDIAN</option>
									<option value="JAWA">JAWA</option>
									<option value="JIALING">JIALING</option>
									<option value="JINLUN">JINLUN</option>
									<option value="JOHNWAY">JOHNWAY</option>
									<option value="KAWASAKI">KAWASAKI</option>
									<option value="KAZUMA">KAZUMA</option>
									<option value="KEEWAY">KEEWAY</option>
									<option value="KINROAD">KINROAD</option>
									<option value="KTM">KTM</option>
									<option value="KYMCO">KYMCO</option>
									<option value="LAMBRETTA">LAMBRETTA</option>
									<option value="LAVERDA">LAVERDA</option>
									<option value="LEXMOTO">LEXMOTO</option>
									<option value="LIFAN">LIFAN</option>
									<option value="LINTEX">LINTEX</option>
									<option value="LML">LML</option>
									<option value="LONGJIA">LONGJIA</option>
									<option value="MIACO">MIACO</option>
									<option value="MALAGUTI">MALAGUTI</option>
									<option value="MASAI">MASAI</option>
									<option value="MATCHLESS">MATCHLESS</option>
									<option value="MBK">MBK</option>
									<option value="MONTESA">MONTESA</option>
									<option value="MORINI">MORINI</option>
									<option value="MOTO GUZZI">MOTO GUZZI</option>
									<option value="MOTO MORINI">MOTO MORINI</option>
									<option value="MOTO-ROMA">MOTO-ROMA</option>
									<option value="MOTORHISPANIA">MOTORHISPANIA</option>
									<option value="MV AGUSTA">MV AGUSTA</option>
									<option value="MZ">MZ</option>
									<option value="NECO">NECO</option>
									<option value="NEW FORCE">NEW FORCE</option>
									<option value="NIPPONIA">NIPPONIA</option>
									<option value="NORTON">NORTON</option>
									<option value="NVT">NVT</option>
									<option value="OSET">OSET</option>
									<option value="PANTHER">PANTHER</option>
									<option value="PEUGEOT">PEUGEOT</option>
									<option value="PGO">PGO</option>
									<option value="PIAGGIO">PIAGGIO</option>
									<option value="PIONEER">PIONEER</option>
									<option value="POLARIS">POLARIS</option>
									<option value="PUCH">PUCH</option>
									<option value="PULSE">PULSE</option>
									<option value="PUZEY">PUZEY</option>
									<option value="QINGQI">QINGQI</option>
									<option value="QUADZILLA">QUADZILLA</option>
									<option value="RELIANT">RELIANT</option>
									<option value="REV-TECH">REV-TECH</option>
									<option value="RIEJU">RIEJU</option>
									<option value="ROXON">ROXON</option>
									<option value="ROYAL ENFIELD">ROYAL ENFIELD</option>
									<option value="SACHS">SACHS</option>
									<option value="SANBEN">SANBEN</option>
									<option value="SANYA">SANYA</option>
									<option value="SCORPA">SCORPA</option>
									<option value="SEA-DOO">SEA-DOO</option>
									<option value="SHERCO">SHERCO</option>
									<option value="SHINERAY">SHINERAY</option>
									<option value="SINNIS">SINNIS</option>
									<option value="SKYJET">SKYJET</option>
									<option value="SKYTEAM">SKYTEAM</option>
									<option value="SPY RACING">SPY RACING</option>
									<option value="STOMP">STOMP</option>
									<option value="SUKIDA">SUKIDA</option>
									<option value="SUPERBYKE">SUPERBYKE</option>
									<option value="SUZUKI">SUZUKI</option>
									<option value="SYM">SYM</option>
									<option value="TAMORETTI">TAMORETTI</option>
									<option value="TGB">TGB</option>
									<option value="TITAN">TITAN</option>
									<option value="TOMOS">TOMOS</option>
									<option value="TRIUMPH">TRIUMPH</option>
									<option value="URBAN">URBAN</option>
									<option value="VESPA">VESPA</option>
									<option value="VICTORY">VICTORY</option>
									<option value="VINCENT">VINCENT</option>
									<option value="WARRIOR">WARRIOR</option>
									<option value="WK BIKES">WK BIKES</option>
									<option value="WUYANG">WUYANG</option>
									<option value="X-SPORT">X-SPORT</option>
									<option value="XBLADE">XBLADE</option>
									<option value="XINGYUE">XINGYUE</option>
									<option value="XINGLING">XINGLING</option>
									<option value="XISPA">XISPA</option>
									<option value="YAMAHA">YAMAHA</option>
									<option value="ZENNCO">ZENNCO</option>
									<option value="ZHEJIANG">ZHEJIANG</option>
									<option value="ZNEN">ZNEN</option>
									<option value="ZONGSHEN">ZONGSHEN</option>
									<option value="ZONTES">ZONTES</option>
									<option value="ZY MOTORS">ZY MOTORS</option>
								</select>
<?php endif; ?>
							</div>
							<div class="input-wrapper">
								<label>Model <span>*</span></label>
<?php if (!empty($VRMDetails['DATA']['Model'])): ?>
								<input class="required" name="model-sel" id="model-sel" type="text" value="<?= $VRMDetails['DATA']['Model']; ?>" />
<?php else: ?>
								<select class="required" id="model-sel" name="model-sel">
									<option value="">Please Select...</option>
								</select>
<?php endif; ?>
							</div>
							<div class="input-wrapper">
								<label>Variant (eg. Bandit)</label>
								<input type="text" name="variant" value="<?= $VRMDetails['DATA']['Variant']; ?>">
							</div>
							<div class="input-wrapper">
								<label>Year <span>*</span></label>
								<select class="required" name="year-inp">
									<option value="">Please select year...</option>
									<?php for ($i = date('Y'); $i >= 1960; $i--) : ?>
										<?php $selected = ($i === $VRMDetails['DATA']['Year'])? 'selected':''; ?>
										<option value="<?php echo $i; ?>" <?= $selected ?>><?php echo $i; ?></option>
									<?php endfor; ?>
								</select>
							</div>
							<div class="input-wrapper">
								<label>Engine Size <span>*</span></label>
								<input class="required" onkeypress="return isAlphaNumberKey(event)" name="engine-size" id="engine-size" type="text" value="<?= $VRMDetails['DATA']['EngineSize']; ?>" />
							</div>
							<div class="input-wrapper">
								<label>Colour <span>*</span></label>
								<input onkeypress="return isAlphaKey(event)" class="required" type="text" name="colour-inp" value="<?= $VRMDetails['DATA']['Colour']; ?>">
							</div>
							<div class="input-wrapper">
								<label>Mileage <span>*</span></label>
								<input onkeypress="return isNumberKey(event)" type="number" name="mileage-inp" value="" placeholder="e.g. 40000" class="required">
							</div>
						</div>

						<div id="form-wrapper" class="two">
							<h3><span class="numbering">2</span>Further Details</h3>
							<div class="input-wrapper">
								<label>M.O.T <span>*</span></label>
								<select class="required" name="mot-sel">
									<option value="">Please Select...</option>
									<option value="Not required">Not required</option>
									<option value="Under 1 month">Under 1 month</option>
									<option value="Over 1 month">Over 1 month</option>
									<option value="MOT expired">MOT expired</option>
								</select>
							</div>
							<div class="input-wrapper">
								<label>Is the Bike a Non-Runner?</label>
								<div class="radiobtnGroup">
									<input type="radio" name="non-runner" value="1" class="radiobtnyes" >
									<span>Yes</span>
									<input type="radio" name="non-runner" value="2" class="radiobtnyes" checked >
									<span>No</span>
									<input type="radio" name="non-runner" value="3" class="radiobtnyes">
									<span>Just needs a battery</span>
								</div>
							</div>
							<div class="input-wrapper">
								<label>Has your Bike ever been the subject to an Insurance write-off? <span>*</span></label>
								<div class="radiobtnGroup">
									<input type="radio" name="insurance-sel" value="1" class="radiobtnyes" >
									<span>Yes</span>
									<input type="radio" name="insurance-sel" value="0" class="radiobtnyes" checked >
									<span>No</span>
								</div>
							</div>
							<div class="input-wrapper">
								<label>How Many Owners?</label>
								<select id="owners-inp" name="owners-inp">
									<option value="">Please Select...</option>
									<?php for ($i = 1; $i <= 10; $i++) : ?>
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
									<?php endfor; ?>
								</select>
							</div>
							<div class="input-wrapper">
								<label>Service History <span>*</span></label>
								<select class="required" id="service-history" name="service-history">
									<option value="">Please Select...</option>
									<option value="Full history">Full history</option>
									<option value="Part history">Part history</option>
									<option value="First not due">First not due</option>
									<option value="No history">No history</option>
								</select>
							</div>
							<div class="input-wrapper">
								<label>How do you rate the bike’s condition? <span>*</span></label>
								<select class="required" id="condition-sel" name="condition-sel">
									<option value="">Please Select...</option>
									<option value="5 - Showroom condition">5 - Showroom condition</option>
									<option value="4 - Good">4 - Good</option>
									<option value="3 - Average">3 - Average</option>
									<option value="2 - Below average">2 - Below average</option>
									<option value="1 - Very poor">1 - Very poor</option>
								</select>
							</div>
                            <div class="input-wrapper">
                                <label>Will the bike come with panniers? <span>*</span></label>
                                <select class="required" id="panniers-sel" name="panniers-sel">
                                    <option value="">Please Select...</option>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
						</div>


                        <div id="form-wrapper" class="three">
							<h3><span class="numbering">3</span>Your Details</h3>

							<div class="input-wrapper">
								<label for="contact-title">Title <span>*</span></label>
								<select class="required" name="contact-title" id="contact-title">
									<option value="">Please Select...</option>
									<option value="Mr">Mr</option>
									<option value="Mrs">Mrs</option>
									<option value="Miss">Miss</option>
									<option value="Ms">Ms</option>
								</select>
							</div>

							<div class="input-wrapper">
								<label>First Name <span>*</span></label>
								<input class="required" onkeypress="return isAlphaKey(event)" name="first-name" id="first-name" type="text">
							</div>
							<div class="input-wrapper">
								<label>Last Name <span>*</span></label>
								<input class="required" onkeypress="return isAlphaKey(event)" name="last-name" id="last-name" type="text">
							</div>
							<div class="input-wrapper">
								<label>Contact Number <span>*</span></label>
								<input data-regex="^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$" class="required" name="phone-inp" id="phone-inp" type="tel"/>
							</div>
							<div class="input-wrapper">
								<label>Email <span>*</span></label>
								<input data-regex="^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$" class="required" name="email-inp" id="email-inp" type="email"/>
							</div>
<!--							<div class="input-wrapper">
								<label>Postcode <span>*</span></label>
								<input pattern="[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}|[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}" type="text" value="" id="post-code" class="required post-code post-code-new" data-source="-new" name="postcode-inp"/><span class="address-lookup FindAddressBtn" data-source="-new">Lookup</span>
							</div>
							<div style="display: none;" class="postcodedropdown autoload-main-new postcodedropdown input-wrapper"></div>

							<div class="input-wrapper">
								<label>Address line 1 <span>*</span></label>
								<input class="required address1-new" name="address1-inp" id="address1-inp" type="text" />
							</div>
							<div class="input-wrapper">
								<label>Address line 2</label>
								<input class="address2-new" name="address2-inp" id="address2-inp" type="text" />
							</div>
							<div class="input-wrapper">
								<label>City / Town <span>*</span></label>
								<input class="required city-new" name="city-inp" id="city-inp" type="text" />
							</div>
							<div class="input-wrapper">
								<label>County <span>*</span></label>
								<input class="required country-new" name="county-inp" id="county-inp" type="text" />
							</div>-->
						</div>

						<div id="form-wrapper" class="four checkbox-simple">
							<h3><span class="numbering">4</span>Terms &amp; Conditions</h3>
							<div class="input-wrapper">
								<input class="required" name="confirm" id="confirm" value="confirm" type="checkbox">
								<label>I confirm I have read and understood the <a href="<?php echo home_url(); ?>/terms-and-conditions/">terms and conditions</a> <span>*</span></label>
							</div>
						</div>
						<div id="submit-wrapper">
							<input name="source" id="source" type="hidden" value="WWB <?php echo $_SESSION['utm_source']; ?>" />
							<input name="campaign" id="campaign" type="hidden" value="WWB <?php echo $_SESSION['utm_campaign']; ?>" />
							<input name="affiliateid" id="affiliateid" type="hidden" value="<?php echo $_SESSION['affiliateid']; ?>" />
							<input name="ppckeyword" id="ppckeyword" type="hidden" value="<?php echo $_SESSION['ppckeyword']; ?>" />
							<input name="ip" id="ip" type="hidden" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>" />
							<input name="division" id="division" type="hidden" value="WeWantBikes" />
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/secure.png" />
							<input type="submit" value="submit »">
                            <p style="clear:both; font-size: 11px; padding-top: 10px">By clicking Submit you are confirming you are happy for us to contact you using the information provided.</p>
						</div>
					</form>

				</div>
			</div>

		</div><!-- #content -->
</div><!-- #primary -->
<script>
	var base_url = '<?php echo bloginfo('url'); ?>';
	jQuery('.address-lookup').click(function() {
		var source = jQuery(this).attr('data-source');
		if (jQuery('.post-code-new').val().length <= 0) {
			jQuery('.autoload-main' + source).hide();
		} else
		{
			jQuery('.autoload-main' + source).show();
		}
		jQuery.ajax({
			url: base_url + '/wp-admin/admin-ajax.php',
			data: {action: 'get_address_list', searchkey: jQuery('.post-code-new').val(), source: source},
			error: function() {
				jQuery('.autoload-main' + source).hide();
				alert('Error looking up address. Please check you postcode and try again...');

			},
			success: function(data) {
				if (data != 0) {
					jQuery('.autoload-main' + source).show();
					jQuery('.autoload-main' + source).html(data);
				} else {
					jQuery('.autoload-main' + source).hide();
					jQuery('.address' + source).attr('value', '');
				}
			},
			type: 'POST'
		});

	});
	jQuery('.autoload-sub').live('change', function() {
	var source = jQuery(this).find(':selected').attr('data-source');



	var addressid = jQuery(this).find(':selected').attr('data-addressid');
		console.log( source );
		console.log( addressid );
	jQuery.ajax({

		url: base_url + '/wp-admin/admin-ajax.php',
		data: {action: 'get_address_detail', searchkey: addressid, source: source},
		error: function() {
			jQuery('.autoload-main' + source).hide();
			alert('Api Token Expired... number 2');
		},
		success: function(data) {

				var jsondata = JSON.parse(data);
				if (jsondata.status == 1) {
				jQuery('.address1' + source).attr('value',jsondata.address1);
				jQuery('.address2' + source).attr('value', jsondata.address2);
				jQuery('.city' + source).attr('value', jsondata.city);
				jQuery('.country' + source).attr('value', jsondata.county);
				jQuery('#addressid'+source).attr('value',addressid);
				jQuery('.autoload-main' + source).hide();

			} else {

				jQuery('.autoload-main' + source).show();
				jQuery('.autoload-main' + source).html("<span style='color:red'>We are unable to fetch addresses due to some technical reasons. Please try with another postcode or input your address details.</span>").fadeOut(5000);
				jQuery('.address' + source).attr('value', '');

			}
		},
		type: 'POST'
	});

	});

function isAlphaKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 32 && charCode > 31
	&& (charCode <65 || charCode >122)) {
		return false;
	}
	return true;
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 46 && charCode > 31
	&& (charCode < 48 || charCode > 57))
		return false;

	return true;
}
function isAlphaNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 32 && charCode > 31
	&& (charCode < 48 || charCode > 57) && (charCode <65 || charCode >122) )
		return false;

	return true;
}

MakeModels = {
	'Adly': 'Activator,Predator,SS,TGB',
	'Aeon': 'Aero,Pulsar',
	'AGS': '16MS,31,Bobber,Daytona,DD,EOS,Firefox,JS,JS125,JSM,LJ,NAC,Regal,SPT,AmericaN Ironhorse,Classic chopper,Outlaw,Tejas',
	'Apache': 'ATV,RLX',
	'Apollo': 'RX',
	'Aprilia': 'AF,Area,Atlantic,Capo,Caponord,Dorsoduro,Futura,Habana,Leonardo,Mana,Mojito,MX,Pegaso,RS,RSV,RSV-R,RX,RXV,Scarabeo',
	'Ariel': 'Arrow,Golden Arrow,VH Red Hunter,Armstrong,MT',
	'Bad Island Toyz': 'Chopper',
	'Baotian': 'BT,Citibike,Eagle,Evolution,falcon,Monza,Speedy,Tanco',
	'Bashan': 'BS200S,Warrior',
	'Beeline': 'Veloce',
	'Benelli': '49,49X,Adiva,Cafe Racer,Pepe,TNT,Tornado,TRE,TRE-K,Velvet',
	'Beta': 'Evo,Rev 3',
	'Big Dog': 'Canine,Pitbull,Ridgeback',
	'Bimota': 'DB,Mantra,SB,YB',
	'BMC': 'Big Daddy,Bobber',
	'BMW': 'C1,C600,C650,F650,F700,F800,G450,G650,HP2,K1,K100,K1100,K1200,K1300,K1600,K75,R100,R1100,R1150,R1200,R45,R65,R80,R850,S1000',
	'Bombardier': 'DS650',
	'Boom': 'Fun500',
	'Boss Hoss': 'BHC-3 LS3',
	'BSA': 'A10,A65,A7,B25,B50,BANTAM,C11,C15,GOLD STAR,M20',
	'BUELL': '1125CR,1125R,BLAST,X1,XB12R,XB12SS,XB12X,XB9R,XB9SX',
	'BULTACO': 'SHERPA',
	'CAGIVA': 'BLUES,CANYON,METO,RAPTOR,RIVER,V RAPTOR',
	'CAN-AM': 'COMMANDER,OUTLANDER,RENEGADE,SPYDER',
	'CCM': '404,604,644,CRCR,CXR,R30,R35,SR',
	'COBRA': 'AX',
	'CPI': 'ARAGON,BRAVO,FORMULA,GTR,SPRINT,SUPERCROSS,SUPERMOTO,SUV',
	'DAELIM': 'B,BESBI,BONE,BONITA,CITI,CORDI,DAYSTAR,DELFINO,NS,ROADWIN,S,S1,VEE,VL,VS',
	'DAFIER': 'GHOST',
	'DERBI': 'CITY CROSS,DRD,GP,GPR,MULHACEN,PREDATOR,SENDA,TERRA',
	'DI-BLASI': 'DI',
	'DINLI': 'DINO',
	'DIRECT BIKE': '125CC,50CC',
	'DKW': 'W2000',
	'DUCATI': '1000,1098,1100,1198,1199,400,600,620,696,748,749,750,796,800,848,888,900,906,916,996,998,999,DESMOCEDICI,DESMOSEDICI,DIAVEL,GT,HYM,HYPERMOTARD,MONSTER,MULTISTRADA,S2R,S4R,ST,STREETFIGHTER',
	'E-MOTIVE': 'E1',
	'EASY RIDER': 'M50',
	'ELECTRICYCLE': 'CITY',
	'ENFIELD': 'BULLET,CLASSIC',
	'FOREVER': 'DXB',
	'FRANCIS BARNETT': 'FALCON',
	'GARELLY': 'KL50',
	'GAS GAS': 'EC,ECF,ENDURO,PRO,RAGA,TX,TXT',
	'GENERIC': 'CRACKER,EPICO,IDEO,RACE,ROC,SOHO,TRIGGER,WORX,XOR',
	'GIANT CO': 'SPRINT',
	'GILERA': 'COGUAR,DNA,FUOCO,GP,GSM,ICE,NEXUS,RCR,RUNNER,STORM',
	'GREEVES': '200',
	'HAOTIAN': 'ARROW',
	'HARLEY DAVISON': 'DEUCE,DYNA,DNYA GLIDE,FAT BOY,FLD,FLHR,FLHRC,FLHRI,FLHRS,FLHRSE3,FLHRSE4,FLHRSI,FLHS,FLHT,FLHTCSE2,FLHTCU,FLHTCUI,FLHTCUSE,FLHTCUSE3,FLHTCUSE7,FLHTK,FLHX,FLS,FLST,FLSTC,FLSTCI,FLSTF,FLSTFB,FLSTFSE,FLSTFSE2,FLSTI,FLSTN,FLSTSC,FLTRUSE,FXCW,FXCWC,FXD,FXDV,FXDVI,FXDC,FXDCI,FXDF,FXDF FATBOB,FXDL,FXDLI,FXDSE,FXDWG,FXDWGI,FXDXI,FXR,FXS,FXST,FXSTB,FXSTBI,FXSTBI NIGHT TRAIN,FXSTC,FXSTI,FXSTSSE2,HARDTAIL,SCREEMIN EAGLE,SOFTTAIL,SPORTSTER,TOURING,VRSC,VSCA,VRSCAW,VSRCB,VRSCD,VRSCDX,VRSCF,VRSCR,VRSCSE2,XL,XL1200,XL833R,XR,XR1000',
	'HI BIRD': 'HBM250V CHOPPER',
	'HONDA': 'AFRICA TWIN,ANF125,ANNOVA,BALI,BROS,C,C50,C90,CA125,CAMINO,CB,CBF,CBR,CBX,CD,CG,CH,CITY,CL,CM,CR,CRF,CROSSRUNNER,CROSSTOURER,CX,DAX,DEAUVILLE,DN,DN01,DOMINATOR,F6,FES,FJS,FMX,FTR,FX,GB,GL,H,HORNET,LEAD,MELODY,MONKEY BIKE,MTX,NC,NR,NRX,NS,NSR,NT,NTV,NV,NX,PAN,PC PACIFIC COAST,PCX,PES,PS,QUAD,RC,REVERE,RUNE,RVF,SES,SFX,SGX,SH,SILVER,SILVERWING,SLR,SPI,SRX,ST,TRANSALP,TRX,VARADERO,VF,VFR,VISION,VT,VTR,WAVE,X11,X8R,XL,XL VARADERO,XLR XR',
	'HONGDU': 'CG,CHITUMA',
	'HUATIAN': 'HT',
	'HUONIAO': 'HN',
	'HUSABERG': '400',
	'HUSQVARNA': '610,CR,HUSKY,NUDA,SM,SMR,TC,TE,WR,WRE',
	'HYOSUNG': 'AQUILA,COMET,GT,GV,MS,RAPIER,RT,RX',
	'INDIAN': 'SCOUT',
	'JAWA': '350',
	'JIALING': 'JD,TD',
	'JINLUN': 'JL,SPARTIAN,TEXAN',
	'JOHNWAY': 'HURRICAN,TORNADO',
	'KAWASAKI': 'AR,BN,CONCOURS,D-TRACKER,EJ,EL,ELIMINATOR,EN,ER,EX,GPX,GPZ,GT,GTR,HI 500,KDX,KE,KFX,KH,KLE,KLF,KLR,KLV,KLX,KMX,KSR,KX,KZ,NINJA,VERSYS,VN,W,Z,ZEPHYR,ZL,ZR,ZRX,ZX,ZXR,ZZR',
	'KAZUMA': 'FALCON,MEERKAT',
	'KEEWAY': 'ARN,CRUISER,FACT,FLASH,HACKER,HURRICANE,LAND CRUISER,MATRIX,PARTNER,PIXEL,RKV,SPEED,SUPERLIGHT,TX',
	'KINROAD': 'CYCLONE,TYPHOON,XT',
	'KTM': '640,690,ADVENTURE,DUKE,ENDURO,EXC,FREERIDE,LC4,RC,RC8,SM,SMC,SUPERDUKE,SUPERENDURO,SUPERMOTO,SX,XC,XCF,XCFW',
	'KYMCO': 'AGILITY,DJ,DOWNTOWN,EGO,HIPSTER,K PIPE,KR,KRSPORT,LIKE,MILER,PEOPLE,PULSAR,SUPER,XCITING,ZING',
	'LAMBRETTA': 'GP,LI,LM',
	'LAVERDA': '650,668,750',
	'LEXMOTO': 'ARIZONA,ARROW,GLADIATOR,LOWRIDE,LSM,MOLLY,RANGER,STREET,TOMMY,TORNADO,VALENCIA,VIXON,XTR',
	'LIFAN': 'AERO,APOLLO,ARIZONA,DRAGSTAR,EDGE,FS 250Z TRIKE,GRIT,HERITAGE,JET,MILAN,MIRAGE,MONKEY BIKE,TRAILBLAZER',
	'LINTEX': 'HT125,HT5022',
	'LML': 'STAR,STAR DELUXE',
	'LONGJIA': 'LJ50QT',
	'MIACO': 'CLASSIC',
	'MALAGUTI': 'F15,PHANTOM',
	'MASAI': 'K',
	'MATCHLESS': 'G18GL',
	'MBK': 'KILIBRE,OVETTO',
	'MONTESA': 'COTA',
	'MORINI': '500',
	'MOTO GUZZI': '1200,750,BELLAGIO,BREVA,CALIFORNIA,GRISO,LE MANS,NEVADA,NEVADA CLASSIC,NORGE,STELVIO,V10 CENTAURO,V11 ,V1200,V50,V62,V7 CLASSIC,V7 RACER,V7 SPECIAL,V7 STONE',
	'MOTO MORINI': '9 1/2,CORSARO,SCRAMBLER',
	'MOTO-ROMA': 'G10,LAMBROS,SK,SMX,SPORT',
	'MOTORHISPANIA': 'RX',
	'MV AGUSTA': '750,BRUTALE,F3,F4',
	'MZ': 'SKORPION',
	'NECO': 'ABRUZZI',
	'NEW FORCE': 'NF',
	'NIPPONIA': 'MIRO',
	'NORTON': 'CLASSIC,COMMANDER,DOMINATOR,F1,INTERNATIONAL',
	'NVT': 'ER',
	'OSET': '12.5 24,16.0 24,16.0 36',
	'PANTHER': '65',
	'PEUGEOT': 'ALYSEO,ELYSTAR,JETFORCE,KISBEE,LOOXOR,LUDIX,LXR,SATELIS,SCOOT,SPEEDFIGHT,SUM-UP,TKR,TREKKER,TWEET,V-CLIC,VIVACITY',
	'PGO': 'G-MAX,LIGERO,PMX,T REX',
	'PIAGGIO': 'B,BEVERLY,CARNABY,ET,FLY,LIBERTY,MP3,NRG,PX,SFERA,T5,TYPHOON,VESPA,X10,X7,X8,X9,XEVO,ZIP',
	'PIONEER': 'TORRO,XF',
	'POLARIS': 'OUTLAW,RZR,SPORTSMAN',
	'PUCH': 'MAXI',
	'PULSE': 'ADRENALINE,ARKTIX,FORCE,GHOST,LIGHTSPEED,PHANTOM,RAGE,RHYTHEM,SCOUT,STRIDER,TORNADO,ZOOM',
	'PUZEY': 'XTR4',
	'QINGQI': '125 4 A',
	'QUADZILLA': '250E,FOURCROSS,BUGGY,BUZZ,CVT,E,ES,PROSHARK,R,RL,RS,SPORT,ST,X,X8,XLC,Z',
	'RELIANT': 'ESTATE,LX',
	'REV-TECH': 'CHOPPER',
	'RIEJU': 'MARATON,MRT,RS2,RS3,TANGO,TOREO-PACIFIC',
	'ROXON': 'P-ONE',
	'ROYAL ENFIELD': '350 CLASSIC,500 CLASSIC,500 CLUBMAN,500 DE-LUXE,500 ELECTRA,500 TRAILS,550S,BULLET',
	'SACHS': 'BEE,MADASS,SPEEDJET',
	'SANBEN': 'TORNADO',
	'SANYA': 'SY125',
	'SCORPA': 'SCORPA',
	'SEA-DOO': 'RXT',
	'SHERCO': '1,25,2,9,250,TRAIL',
	'SHINERAY': 'XY125',
	'SINNIS': 'APACHE,EAGLE,ECO CITY,FALCON,FLAIRE,HAWK,MATRIX,MAX,PHOENIX,SPIRIT,STEALTH,SUBLINE,TRACKSTAR,VISTA',
	'SKYJET': 'SJ,XGJ',
	'SKYTEAM': 'ACE,MONKEY,PBR,SKYMAX,SM,TR',
	'SPY RACING': 'F1',
	'STOMP': 'JUICE BOX,MOPED,SUPERSTOMP',
	'SUKIDA': 'SK',
	'SUPERBYKE': 'RIDGERIDER,RMR,ROUGHBACK',
	'SUZUKI': 'A100,ADDRESS,AE,AN,ANBURGMAN,AP,AY,DL,DR,EN,FL,GN,GS,GSF,GSR,GSX,GSXR,GT,GZ,INAZUMA,INTRUDER,KINGQUAD,LT,LTZ,M800,RF,RG,RGV,RL-250,RM,RMX,RMZ,RV,SAVAGE,SB,SFV,SP,SV,T500,TL,TR,TS,TU,UC,UG,UH,UX,V-STROM,VAN VAN,VL,VS,VX,VZ,XF',
	'SYM': 'ALLO,CITY COMM,CITY TREK,COMBIZ,DD50,FIDDLE,GTS,HD,HD2,HUSKY,JET,JOYRIDE,MAXSYM,MIO,ORBIT,SHARK,SPORT,SYMBA,SYMPHONY,SYMPLY,TONIK,VOYAGER,VS,WOLF,XS',
	'TAMORETTI': 'RETRO',
	'TGB': '101R,202,302,303R,304,404,BLADE,DELIVERY,R125X,R50X,TARGET,X-MOTION',
	'TITAN': 'SCORPION',
	'TOMOS': 'A3',
	'TRIUMPH': '3TA,500,ADVETURER,AMERICA,BONNEVILLE,DAYTONA,EXPLORER,LEGEND,ROCKET,RUSHDEN,SCRAMBLER,SPEED,SPEEDMASTER,SPRINT,STREET,T,THRUXTON,THUNDERBIRD,TIGER,TRIDENT,TROPHY,TT',
	'URBAN': 'DZ',
	'VESPA': 'ET,GS,GT,GTS,GTV,LX,LXV,P,PK,PX,S,SUPER,T5',
	'VICTORY': 'CLASSIC,CORY NESS,CORY NESS JACKPOT,CROSS,CRUISER,HAMMER,JACKPOT,KINGPIN,V92C,VEGAS,VISION',
	'VINCENT': 'BLACK SHADOW',
	'WARRIOR': 'DESPATCH',
	'WK BIKES': 'CF,GO,GP,JETMAX,ONE,R,RR,SC,SM,SPORT,TRAIL',
	'WUYANG': 'WY',
	'X-SPORT': 'MINI X',
	'XBLADE': 'AGILITY,EL NINO,F,VETERANO,X6',
	'XINGYUE': 'XY',
	'XINGLING': 'XL',
	'XISPA': 'TRIAL',
	'YAMAHA': '350,AEROX,BANSHEE,BT,BWS,CS50,CW BWIZZ,DIVERSION,DT,EW,FJ,FSI,FZ,FZR,FZS,GIGGLE,GRISLY,GTS,JOG,MT,NXC,PW,PW50,PW80,RAPTOR,RD,RS,RXS,RZ,SR,SRX,TDM,TDR,TEOS,TRX,TTR,TW,TY,TZR,V MAX,V STAR,VITY,VMAX,VP,WAVE,WR,X-CITY,X-MAX,XC,XENTER,XJ,XJR,XP,XS,XT,XTZ,XV,XVS,XVZ,YBR,YFM,YH,YN,YP,YQ,YX,YZ,YZF',
	'ZENNCO': 'BULLET,SATURN',
	'ZHEJIANG': '250',
	'ZNEN': 'FIRENZE',
	'ZONGSHEN': 'PREDATOR',
	'ZONTES': 'MONSTER,PANTHER,TIGER',
	'ZY MOTORS': 'ZY125'
};

function FillModels(obj) {
	var ModelsString = MakeModels[jQuery(obj).val()];
	if (ModelsString == undefined)
		return;

	jQuery('#model-sel').empty().append('<option selected="selected" value="">Please Select...</option>');
	var values = ModelsString.toString().split(',');
	for (i = 0; i < values.length; i++) {
		jQuery('#model-sel').append(jQuery("<option>").attr('value',values[i]).text(values[i]));
	}
}

jQuery(document).ready(function() {

	jQuery("input.required, select.required").blur(function(){
		if (jQuery(this).val() == ""){
			jQuery(this).parent().addClass('failedvalidation');
		}
		else {
			var regex = new RegExp(jQuery(this).data('regex'));
			var value = jQuery(this).val();
			if (regex !== undefined) {
				if (regex.test(value) != true) {
					jQuery(this).parent().addClass('failedvalidation');
				}
				else {
					jQuery(this).parent().removeClass('failedvalidation');
				}
			}
			else {
				jQuery(this).parent().removeClass('failedvalidation');
			}
		}
	});

	jQuery("#enquiry-form").submit(function(event){
		var passedvalidation = "true";
		jQuery("#enquiry-form input.required, #enquiry-form select.required").each(function( index, element ) {
		if (jQuery(this).val() == ""){
				jQuery(this).parent().addClass('failedvalidation');
				passedvalidation = "false";
			}
			else {
				var regex = new RegExp(jQuery(this).data('regex'));
				var value = jQuery(this).val();
				if (regex !== undefined) {
					if (regex.test(value) != true) {
						jQuery(this).parent().addClass('failedvalidation');
						passedvalidation = "false";
					}
					else {
						jQuery(this).parent().removeClass('failedvalidation');
					}
				}
			}
		});
		if(!jQuery('#confirm').is(':checked')) {
			jQuery("#confirm").parent().addClass('failedvalidation');
			passedvalidation = "false";
		}
		else {
			jQuery("#confirm").parent().removeClass('failedvalidation');
		}
		if (passedvalidation == "false"){
			event.preventDefault ? event.preventDefault(event) : event.returnValue = false;
			alert("Please complete all required fields.");
		}
	});

});

</script>
<?php
get_footer();

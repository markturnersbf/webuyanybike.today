<?php

/**
 * The VRMLookupHandler function takes a single parameter.
 * Either true or false, it defaults to false. Setting it
 * to true will show any internal error messages.
 *
 *
 * USEAGE:
 * Calling it like this:
 * $VRMDetails = VRMLookupHandler();
 *
 * Will produce one of the two following arrays, or false
 * if there was an internal problem:
 *
 * $VRMDetails['DATA'] if it could find the data
 * $VRMDetails['ERROR'] if there was a problem reported by automotivemxin
 *
 *
 * NOTES:
 * - Wasn't sure which type of year you wanted see line 87
 *   above for the possibilities.
 * - If it's passed an empty MXIN_VRM field the function will return false.
 * - The function takes the MXIN_VRM field from either POST data, like in
 *   a form or from the GET data like in the URL eg ?MXIN_VRM=REGHERE
 *   The POST type will take precedence if both are present
 * - I moved the credentials to an array above (lines 17-21) so they aren't
 *   in the HTML source where every one can see them
 *
 *
 * ERRORS:
 * Depending on the environment your in you might need
 * the following to get errors to actually show:
 * error_reporting(E_ALL);
 * ini_set('display_errors', true);
 *
 *
 * @param bool $debug
 * @return array|bool
 * @throws Exception
 */
function VRMLookupHandler($debug = false)
{

	// Setup
	$requestURL = "https://www.automotivemxin.com/UAT/";
	$requestFieldVRM = (!empty($_POST['MXIN_VRM'])) ? $_POST['MXIN_VRM'] : $_GET['MXIN_VRM'];

	// Check we have been passed the MXIN_VRM field
	if (empty($requestFieldVRM)) {
		if ($debug === true) {
			throw new Exception("Undefined VRM");
		}
		return false;
	}

	//Add the lookup specific fields below.
	$requestFieldsArray["ESERIES_FORM_ID"]				= "B2INT";
	$requestFieldsArray["MXIN_USERNAME"]				= "AUTOMXINUATF0984";
	$requestFieldsArray["MXIN_PASSWORD"]				= "YXNAD0M3";
	$requestFieldsArray["MXIN_PAYMENTCOLLECTIONTYPE"]	= "02";
	$requestFieldsArray["MXIN_TRANSACTIONTYPE"]			= "03";
	$requestFieldsArray["MXIN_VRM"]						= $requestFieldVRM;

	// Format the data into a form that cURL accepts
	$requestFieldsArrayLength = count($requestFieldsArray);
	$requestFieldsString = http_build_query($requestFieldsArray);

	// Open connection
	$ch = curl_init();

	// Setup cURL
	curl_setopt($ch, CURLOPT_URL, $requestURL); // Set the url
	curl_setopt($ch, CURLOPT_POST, $requestFieldsArrayLength); // Count the number of POST fields we're sending
	curl_setopt($ch, CURLOPT_POSTFIELDS, $requestFieldsString); // The POST data to send
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Tell cURL to give us the data from the response

	// Execute post request
	$requestResponse = curl_exec($ch);

	// Get some information about the request
	$requestInfo = curl_getinfo($ch);

	// Check  if we got a HTTP 200 OK
	if ($requestInfo['http_code'] !== 200) {
		if ($debug === true) {
			throw new Exception("cURL received the HTTP code " . $requestInfo['http_code']);
		}
		return false;
	}

	// Check the request was completed successfully
	if (curl_errno($ch) !== 0) {
		if ($debug === true) {
			throw new Exception("cURL encountered a problem\n[" . curl_errno($ch) . "]: " . curl_error($ch));
		}
		return false;
	}

	// Close the connection
	curl_close($ch);

	// Parse the returned string into XML
	$xml = simplexml_load_string($requestResponse);

	// Check the request was completed successfully
	if ($xml === false) {
		if ($debug === true) {
			throw new Exception("Unable to parse returned XML:\ncURL Response:\n\n" . $requestResponse);
		}
		return false;
	}

	// Check if messages were returned...
	$messageArray = (array)$xml->REQUEST->MXE1;
	$messageArray['MSG'] = '<p class="error-message">'.$messageArray['MSG'].'</p>';

	if(count($messageArray) > 1) {
		return array("ERROR" => $messageArray);
	}

	// Extract the details we want
	$details = array();
	$details['VRM'] = (string)trim($xml->REQUEST->MB01->VRM);
	$details['EngineSize'] = (int)$xml->REQUEST->MB01->ENGINECAPACITY;
	$details['Year'] = (int)substr((int)$xml->REQUEST->MB01->DATEFIRSTREGISTERED, 0, 4); // UKDATEFIRSTREGISTERED | DATEFIRSTREGISTERED
	$details['Make'] = (string)$xml->REQUEST->MB01->MAKE;
	$details['Model'] = (string)$xml->REQUEST->MB01->MODEL;
	$details['Colour'] = (string)$xml->REQUEST->MB01->COLOUR;
	//$details['Variant'] = (string)$xml->REQUEST->MB01->??? // Variant doesn't seem to be included in the feed?

	return array("DATA" => $details);

}

?>
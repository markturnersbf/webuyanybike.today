<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

		</div><!-- #main -->

		<footer id="masthead" class="site-header" role="banner">
			<div id="inner-container">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" /></a>
				<a class="sell-head" href="<?php echo home_url(); ?>/enquiry-form">sell my bike &raquo;</a>
				<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
					<button class="menu-toggle"><?php _e( 'Primary Menu', 'twentyfourteen' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'twentyfourteen' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
				</nav>
			</div>
		</footer><!-- #masthead -->
		<div id="row" class="footer blank">
			<div id="inner-container">
				<p class="copyright">© WeBuyAnyBike.Today Copyright 2019. All rights reserved.</p>
				<p class="links"><a href="<?php echo home_url(); ?>/terms-and-conditions" title="Terms and Conditions">Terms and Conditions</a> &nbsp;| &nbsp;<a href="<?php echo home_url(); ?>/privacy-policy" title="Privacy Policy">Privacy Policy</a>&nbsp;| &nbsp;<a href="<?php echo home_url(); ?>/cookie-policy" title="Cookie Policy">Cookie Policy</a></p>
			</div>
		</div>
	</div><!-- #page -->

	<?php wp_footer(); ?>

</body>
</html>

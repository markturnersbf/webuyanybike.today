<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="content-wrapper">
		<div id="main-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

				endwhile;
			?>
			<div id="row" class="">
				<div id="inner-container">
				<div class="reg-wrapper standalone">
					<p class="reg-intro">Enter your reg number to get a <span>FREE</span> valuation...</span></p>
					<form id="reg-input" action="<?php echo home_url(); ?>/enquiry-form/">
						<div class="reg-input">
							<input type="text" name="MXIN_VRM">
						</div>
						<input type="submit" value="value my bike &raquo;" />
					</form>
					<p class="dont-know">Don't know your bike's registration number? <a href="<?php echo home_url(); ?>/enquiry-form/">Click here &raquo;</a></p>
				</div>
			</div>
			</div>

		</div><!-- #content -->
</div><!-- #primary -->
<?php
get_footer();

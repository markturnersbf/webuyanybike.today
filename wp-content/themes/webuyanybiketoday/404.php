<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="content-wrapper">
		<div id="main-content" role="main">
			<div id="row" class="blank">
		<div id="inner-container" class="blog-wrap indexpage">
			
				<h1 ><?php _e( '404 Not Found', 'twentyfourteen' ); ?></h1>
			<?php get_sidebar(); ?>

			<article class="content-none">

<div class="entry-content">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

				<?php get_search_form(); ?>
			</div><!-- .page-content -->
</article>
		
		
		</div>
			</div>
			
			<div id="row" class="">
				<div id="inner-container">
				<div class="reg-wrapper standalone">
					<p class="reg-intro">Enter your reg number to get a <span>FREE</span> valuation...</span></p>
					<form id="reg-input" action="<?php echo home_url(); ?>/enquiry-form/">
						<div class="reg-input">
							<input type="text" name="regnum" />
						</div>
						<input type="submit" value="value my bike &raquo;" />
					</form>
					<p class="dont-know">Don't know your bike's registration number? <a href="<?php echo home_url(); ?>/enquiry-form/">Click here &raquo;</a></p>
				</div>
			</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();
<?php
ini_set('display_errors', 0);
ini_set('error_reporting', E_NONE);

function email_is_correct($user_email){
	if(preg_match("%^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z])+$%", $user_email)) {
		return true;
	}
	else {
		return false;
	}
}

if($_SERVER['REQUEST_METHOD'] == "POST") {

	$fullname = isset($_POST['fullname']) ?  $_POST['title'].' '.$_POST['fullname'].' '.$_POST['lastname'] : '';
	$address = isset($_POST['postcode']) ? $_POST['address1new'].' '.$_POST['address2-new'] : '';
	$postcode = isset($_POST['postcode']) ? $_POST['postcode'] : '';
	$propset = isset($_POST['propset']) ? $_POST['propset'] : '';
	$emailaddr = isset($_POST['emailaddr']) ? $_POST['emailaddr'] : '';
	$phonenumber = isset($_POST['phonenumber']) ? str_replace(' ', '',$_POST['phonenumber']) : '';
	$dateofbirth = isset($_POST['dateofbirthday']) && isset($_POST['dateofbirthmonth']) && isset($_POST['dateofbirthyear']) ? $_POST['dateofbirthmonth'].'/'.$_POST['dateofbirthday'].'/'.$_POST['dateofbirthyear'] : '';
	$amttofin = isset($_POST['amttofin']) ? $_POST['amttofin'] : '';
	$depavl = isset($_POST['depavl']) ? $_POST['depavl'] : '';
	$emplstatus = isset($_POST['emplstatus']) ? $_POST['emplstatus'] : '';
	$lenghtempl = isset($_POST['lofe_year']) ? $_POST['lofe_year'] : '';
	$employername = isset($_POST['employername']) ? $_POST['employername'] : '';
	$ppckeyword = isset($_POST['ppckeyword']) ? $_POST['ppckeyword'] : '';

	$mnttax = isset($_POST['mnttax']) ? $_POST['mnttax'] : '';
	$drvlicence = isset($_POST['drvlicence']) ? $_POST['drvlicence'] : '';
	$crdhist = isset($_POST['crdhist']) ? $_POST['crdhist'] : '';

	$mlstatus = isset($_POST['mlstatus']) ? $_POST['mlstatus'] : '';
	$tcaddress = isset($_POST['tcaddress']) ? $_POST['tcaddress'] : '';

	$tpaddress = isset($_POST['tpaddress']) ? $_POST['tpaddress'] : '';
	$p_address = isset($_POST['prevaddress']) ? $_POST['prevaddress'] : '';
	$p_postcode = isset($_POST['prevpostcode']) ? $_POST['prevpostcode'] : '';

	$lenghtpempl = isset($_POST['lofpe_year']) ? $_POST['lofpe_year'] : '';
    $panniers = isset($_POST['panniers-sel']) ? $_POST['panniers-sel'] : '';
	$prevemployername = isset($_POST['employerprevname']) ? $_POST['employerprevname'] : '';

	$biketofinance = isset($_POST['biketofinance']) ? $_POST['biketofinance'] : '';

	$campaign = isset($_POST['campaign']) ? $_POST['campaign'] : '';
	$source = isset($_POST['source']) ? $_POST['source'] : '';
	$affiliateid = isset($_POST['affiliateid']) ? $_POST['affiliateid'] : '';
	$callbackurl = isset($_POST['callbackurl']) ? $_POST['callbackurl'] : '/';
	$ip = isset($_POST['ip']) ? $_POST['ip'] : '';

	$marketing = isset($_POST['marketing']) ? $_POST['marketing'] : '';
	if(!isset($_POST['marketing'])) {$_POST['marketing']="NO";}

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, 'https://legacy.wewantyourmotorbike.com/receiver-wwb.php');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($_POST));
	$output = curl_exec($curl);
	curl_close($curl);

	header('Location: http://www.webuyanybike.today/thank-you/');

}
?>

<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="content-wrapper">
		<div id="main-content" role="main">
			<div id="row" class="blank">
		<div id="inner-container" class="blog-wrap indexpage">

			<?php if ( have_posts() ) : ?>

			
				<h1><?php printf( __( 'Search for: %s', 'twentyfourteen' ), get_search_query() ); ?></h1>
		

				<?php
					get_sidebar(); 
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next post navigation.
					twentyfourteen_paging_nav();

				else :?>
					<h1>Nothing Found</h1>
						<?php
					get_sidebar(); 
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>

			</div>
			</div>
			
			<div id="row" class="">
				<div id="inner-container">
				<div class="reg-wrapper standalone">
					<p class="reg-intro">Enter your reg number to get a <span>FREE</span> valuation...</span></p>
					<form id="reg-input" action="<?php echo home_url(); ?>/enquiry-form/">
						<div class="reg-input">
							<input type="text" name="regnum" />
						</div>
						<input type="submit" value="value my bike &raquo;" />
					</form>
					<p class="dont-know">Don't know your bike's registration number? <a href="<?php echo home_url(); ?>/enquiry-form/">Click here &raquo;</a></p>
				</div>
			</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();
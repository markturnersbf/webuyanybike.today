<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>

<div id="content-wrapper">
		<div id="main-content" role="main">
			<div id="row" class="blank">
		<div id="inner-container" class="blog-wrap indexpage">
		<?php
			

			 if ( have_posts() ) : ?>

				<h1>
					<?php
						if ( is_day() ) :
							printf( __( 'Archives: %s', 'twentyfourteen' ), get_the_date() );

						elseif ( is_month() ) :
							printf( __( 'Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );

						elseif ( is_year() ) :
							printf( __( 'Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );

						else :
							_e( 'Archives', 'twentyfourteen' );

						endif;
					?>
				</h1>
			
			<?php
				get_sidebar(); 
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

			endif; ?>
			
				</div>
			</div>
			
			<div id="row" class="">
				<div id="inner-container">
				<div class="reg-wrapper standalone">
					<p class="reg-intro">Enter your reg number to get a <span>FREE</span> valuation...</span></p>
					<form id="reg-input" action="<?php echo home_url(); ?>/enquiry-form/">
						<div class="reg-input">
							<input type="text" name="regnum" />
						</div>
						<input type="submit" value="value my bike &raquo;" />
					</form>
					<p class="dont-know">Don't know your bike's registration number? <a href="<?php echo home_url(); ?>/enquiry-form/">Click here &raquo;</a></p>
				</div>
			</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();

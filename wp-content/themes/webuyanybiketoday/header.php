<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
// Start the session
session_start();
if(!isset($_SESSION['utm_source']))
{
	$_SESSION['utm_source'] = '';
	if(isset($_SERVER['HTTP_REFERER']))
		$_SESSION['utm_source'] = $_SERVER['HTTP_REFERER'];
	if(isset($_GET['utm_source']))
		$_SESSION['utm_source'] = $_GET['utm_source'];
	if($_SESSION['utm_source'] == '')
		$_SESSION['utm_source'] = 'Direct';
}
if(!isset($_SESSION['utm_campaign']))
{
	$_SESSION['utm_campaign'] = '';
	if(isset($_GET['utm_campaign']))
		$_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}
if(!isset($_SESSION['affiliateid']))
{
	$_SESSION['affiliateid'] = '38';
}
if(isset($_GET['utm_source'])) {
	$_SESSION['utm_source'] = $_GET['utm_source'];
}
if(isset($_GET['utm_campaign'])) {
	$_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}
if(isset($_GET['affiliateid'])) {
	$_SESSION['affiliateid'] = $_GET['affiliateid'];
}
if(isset($_GET['ppckeyword'])) {
	$_SESSION['ppckeyword'] = $_GET['ppckeyword'];
}
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5QH3SB8');</script>
<!-- End Google Tag Manager -->

<?php if (is_front_page()) : ?>
	<meta name="msvalidate.01" content="AB0F315C0AE4283989C2DB4FC674CB64" />
<?php endif; ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=0.75, minimum-scale=0.75, maximum-scale=0.75, user-scalable=no">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>

	<!-- Facebook Tracking Code -->
	<script>(function() {
		var _fbq = window._fbq || (window._fbq = []);
		if (!_fbq.loaded) {
		var fbds = document.createElement('script');
		fbds.async = true;
		fbds.src = '//connect.facebook.net/en_US/fbds.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(fbds, s);
		_fbq.loaded = true;
		}
		_fbq.push(['addPixelId', '124941314508700']);
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', 'PixelInitialized', {}]);
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=124941314508700&amp;ev=PixelInitialized" /></noscript>
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QH3SB8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<div id="inner-container">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" /></a>
			<a class="sell-head" href="<?php echo home_url(); ?>/enquiry-form">sell my bike &raquo;</a>
			<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
				<button class="menu-toggle"><?php _e( 'Primary Menu', 'twentyfourteen' ); ?></button>
				<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'twentyfourteen' ); ?></a>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>
	</header><!-- #masthead -->

	<div id="main" class="site-main">

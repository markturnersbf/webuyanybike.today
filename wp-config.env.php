<?php
/**
 * Setup environments
 * 
 * Set environment based on the current server hostname, this is stored
 * in the $hostname variable
 * 
 * You can define the current environment via: 
 *     define('WP_ENV', 'production');
 * 
 */


// Set environment based on hostname
switch ($hostname) {
    case 'local.wewantbikes.com':
        define('WP_ENV', 'development');
        break;
    
    case 'wewantbikes.elementalwebdesign.co.uk':
        define('WP_ENV', 'staging');
        break;

    case 'www.webuyanybike.today':
    default: 
        define('WP_ENV', 'production');
}

